package com.example.rentit.sales.domain;

public enum POStatus {
    PENDING, REJECTED, OPEN, CLOSED, PENDING_EXTENSION
}
