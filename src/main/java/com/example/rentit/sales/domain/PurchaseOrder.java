package com.example.rentit.sales.domain;

import com.example.rentit.common.domain.BusinessPeriod;
import com.example.rentit.inventory.domain.model.PlantInventoryEntry;
import com.example.rentit.inventory.domain.model.PlantReservation;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(force=true,access= AccessLevel.PROTECTED)
public class PurchaseOrder {
    @Id
    @GeneratedValue
    Long id;

    @OneToMany
    List<PlantReservation> reservations;

    @ManyToOne
    PlantInventoryEntry plant;

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Column(precision=8,scale=2)
    BigDecimal total;

    @Enumerated(EnumType.STRING)
    POStatus status;

    @Embedded
    BusinessPeriod rentalPeriod;

    @ElementCollection
    List<POExtension> extensions = new ArrayList<>();

    public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period) {
        PurchaseOrder po = new PurchaseOrder();
        po.plant = entry;
        po.rentalPeriod = period;
        po.reservations = new ArrayList<>();
        po.issueDate = LocalDate.now();
        po.status = POStatus.PENDING;
        return po;
    }

    public void setStatus(POStatus newStatus) {
        status = newStatus;
    }

    public void addReservation(PlantReservation reservation) {
        reservations.add(reservation);
    }

    public void requestExtension(POExtension extension) {
        extensions.add(extension);
        status = POStatus.PENDING_EXTENSION;
    }

    public LocalDate pendingExtensionEndDate() {
        if (extensions.size() > 0) {
            POExtension openExtension = extensions.get(extensions.size() - 1);
            return openExtension.getEndDate();
        }
        return null;
    }

    public void acceptExtension(PlantReservation reservation) {
        reservations.add(reservation);
        status = POStatus.OPEN;
        rentalPeriod = BusinessPeriod.of(rentalPeriod.getStartDate(), reservation.getSchedule().getEndDate());
        // UPDATE PO total!!
    }

}
