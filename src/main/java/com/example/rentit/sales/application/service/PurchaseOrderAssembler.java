package com.example.rentit.sales.application.service;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.rentit.sales.application.dto.PurchaseOrderDTO;
import com.example.rentit.sales.domain.PurchaseOrder;
import com.example.rentit.sales.rest.SalesRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Service
public class PurchaseOrderAssembler extends RepresentationModelAssemblerSupport<PurchaseOrder, PurchaseOrderDTO> {

    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;

    public PurchaseOrderAssembler() {
        super(SalesRestController.class, PurchaseOrderDTO.class);
    }

    @Override
    public PurchaseOrderDTO toModel(PurchaseOrder po) {
        PurchaseOrderDTO dto = createModelWithId(po.getId(), po);
        dto.setStatus(po.getStatus());
        dto.set_id(po.getId());
        dto.setRentalPeriod(BusinessPeriodDTO.of(po.getRentalPeriod().getStartDate(), po.getRentalPeriod().getEndDate()));
        dto.setPlant(plantInventoryEntryAssembler.toModel(po.getPlant()));

        dto.add(linkTo(methodOn(SalesRestController.class)
                .fetchPurchaseOrder(dto.get_id())).withRel("fetch"));
        try {
            switch (po.getStatus()) {
                case PENDING:
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .acceptPurchaseOrder(dto.get_id())).withRel("accept"));
                    dto.add(linkTo(methodOn(SalesRestController.class)
                            .rejectPurchaseOrder(dto.get_id())).withRel("reject"));
                    break;
                default:
                    break;
            }
        } catch (Exception e) {}

        return dto;
    }
}
