package com.example.rentit.sales.application.dto;

import com.example.rentit.common.application.dto.BusinessPeriodDTO;
import com.example.rentit.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.rentit.sales.domain.POStatus;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;


@Data
public class PurchaseOrderDTO extends RepresentationModel<PurchaseOrderDTO> {
    Long _id;
    BusinessPeriodDTO rentalPeriod;
    PlantInventoryEntryDTO plant;
    POStatus status;
}
